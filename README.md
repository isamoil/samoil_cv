# Samoil Ion CV

## Installation

1. Run `docker network create --subnet=172.2.0.0/24 --ip-range=172.2.0.0/24 --gateway=172.2.0.1 web`
2. Update docker-compose.yml: device path
3. Run `docker-compose up --build`
4. Run `docker exec -it samoil-cv-php bash`
   1. Inside container run `composer install`
5. update hosts file `sudo gedit /etc/hosts` with
   `172.2.0.4  samoil-cv.loc`


### For each database changes (Entity modification)
1. Run `docker exec -it samoil-cv-php bash`
2. Inside container run 
   1. To generate migrations automatically - `composer generate`
   2. To load migrations into sql - `composer migrate`

