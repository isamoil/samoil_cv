<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220621091317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT  EXISTS  about (id INT AUTO_INCREMENT NOT NULL, position VARCHAR(100) NOT NULL, firstname VARCHAR(100) NOT NULL, lastname VARCHAR(100) NOT NULL, description LONGTEXT NOT NULL, intuition INT NOT NULL, creativity INT NOT NULL, luck INT NOT NULL, awesomeness INT NOT NULL, image LONGTEXT NOT NULL, location VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, birth_date VARCHAR(255) NOT NULL, web_site VARCHAR(255) NOT NULL, linkedin VARCHAR(255) NOT NULL, instagram VARCHAR(255) NOT NULL, facebook VARCHAR(255) NOT NULL, twitter VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT  EXISTS educations (id INT AUTO_INCREMENT NOT NULL, start_date VARCHAR(30) NOT NULL, end_date VARCHAR(30) NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, logo LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT  EXISTS experiences (id INT AUTO_INCREMENT NOT NULL, start_date VARCHAR(30) NOT NULL, end_date VARCHAR(30) NOT NULL, position VARCHAR(255) NOT NULL, company_name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT  EXISTS services (id INT AUTO_INCREMENT NOT NULL, documentations VARCHAR(255) NOT NULL, fast_codding VARCHAR(255) NOT NULL, online_pressentions VARCHAR(255) NOT NULL, online_shops VARCHAR(255) NOT NULL, image_redator VARCHAR(255) NOT NULL, video_redator VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT  EXISTS skills (id INT AUTO_INCREMENT NOT NULL, description LONGTEXT NOT NULL, symfony INT NOT NULL, angular INT NOT NULL, react_js INT NOT NULL, php INT NOT NULL, js INT NOT NULL, flutter INT NOT NULL, css INT NOT NULL, years_experience INT NOT NULL, folowers_linkedin INT NOT NULL, projects INT NOT NULL, clients INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE IF EXISTS about');
        $this->addSql('DROP TABLE IF EXISTS educations');
        $this->addSql('DROP TABLE IF EXISTS experiences');
        $this->addSql('DROP TABLE IF EXISTS services');
        $this->addSql('DROP TABLE IF EXISTS skills');
        $this->addSql('DROP TABLE IF EXISTS user');
    }
}
