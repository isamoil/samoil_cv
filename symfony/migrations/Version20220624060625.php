<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220624060625 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO about (`position`,firstname,lastname,description,intuition,creativity,luck,awesomeness,image,location,email,phone_number,birth_date,web_site,linkedin,instagram,facebook,twitter) VALUES
    ('Fullstack Developer','Ion','Samoil','I am incredibly happy that after so many years of programming, I have kept that first feeling of amazement offered by programming, that feeling of hunger & the desire to learn as much as possible today, so that tomorrow I will be abel to apply in practice.',80,75,50,90,'1655995480.jpg','Chișinău, Moldova','ion.samoil@gmail.com','079897205','14.02.1996','-','https://www.linkedin.com/in/ion-samoil-633393161/','https://www.instagram.com/ion.samoil.14/','https://www.facebook.com/ion.samoil.14','https://twitter.com/?lang=en')");
        $this->addSql("
        INSERT INTO educations (start_date,end_date,name,description,logo,owner_id) VALUES
	 ('2004','2012','Chirianca Gymnasium, Village Chirianca','A primary school is a place where children learn to perform new activities. A child participates in different games and becomes open to more opportunities. The requirements of the child are slowly brought to the attention of the teacher.','1655898407.jpg',1),
	 ('2012','2015','Professional School no.4, Mun. Chișinău','Speciality \"Computer operator\". Firsts stepts in IT career.  The place where it is very important not to lose interest in the general objects of study and to want to take the baccalaureate and after starting your studies at the University','1655898407.jpg',1),
	 ('2015','2016','\"Ion Vatamanu\" Theoretical High School, City Strășeni','A year in which I did not have much time for adaptation, but which was successfully completed, mainly due to the completion of the baccalaureate exams. However, another factor can be the friendship with new people!','1655898407.jpg',1),
	 ('2016','2020','License: Technical University of Moldova, City Chișinău','Faculty of Computers, Informatics and Microelectronics, Bachelor Programme, Speciality \"Computers\". The best IT faculty in Moldova. Hundreds of students graduate from college each year and fill various positions in IT companies.','1655898407.jpg',1),
	 ('2020','2022','Master: Technical University of Moldova, City Chișinău','Faculty of Computers, Informatics and Microelectronics, Master Programme, Speciality \"Computers and Networks\”. The master''s decision represents a future perspective.','1655898407.jpg',1),
	 ('2020','2022','Master: Ștefan cel Mare University of Suceava','Faculty of Electrical Engineering and Computer Science, Master Programme, Speciality \"Communication Networks and Computers\”. European engineer.','1655898407.jpg',1);");
        $this->addSql("INSERT INTO experiences (`position`,company_name,description,owner_id,logo,period) VALUES
	 ('Full stack web engineer','Tekoway(ArtSintez Media SRL), City Chișinău','Tekoway is a full stack technology company, who acompanies you on all your digital projects: web applications, mobile applications or business tools.  Be it for your company or for one of your clients, we put at your disposal our agile team of 35 experts passionated about technology, and will offer you a tailor-made development.',1,'1655989006.png','2018 - Present'),
	 ('Full stack web engineer','Digital Virgo France, City Paris','The Digital Virgo Group is one of the world''s leading specialists in mobile payment via Telecom Operators’ billing solutions. By connecting Merchants to Carriers, we address the growing need for the digitalisation of payment by using a simple, fast and secure transactional channel available anywhere in the world. The Group''s added value lies in its ability to address mobile payment in its entirety to optimize the billing by considering strategic aspects such as the customer journey, local adaptation, user acquisition, the data management or the regulatory and compliance framework.',1,'1655989057.jpg','2020 - Present');");

        $this->addSql("INSERT INTO skills (description,symfony,angular,react_js,php,js,flutter,css,years_experience,folowers_linkedin,projects,clients,owner_id) VALUES
	 ('My first professional contract was offered to me by the Tekoway company, where I have been growing & learning, from interesting projects & from super & wonderful colleagues. It all started with a common practice at the university, but which over time has reached years of collaboration. Surely everything has to start with a huge hunger of experience, knowledge & results, but professionals & patient mentors are also needed, which my company has certainly given me. I''m full stack dev, I like that because it''s a charm when you see the whole picture, but not just parts of it.',88,90,65,80,85,40,50,4,350,15,7,1);");
        $this->addSql("INSERT INTO services (documentations,fast_codding,online_pressentions,online_shops,dev_ops,audit,owner_id) VALUES
	 ('It’s hard to create great documentation. Working on it often means ignoring another part of your job—and yet that time can be just as valuable as your development work. A few hours a week spent improving documentation can have a compounding effect. Developers will get stuck less frequently, there will be fewer support requests, and hopefully fewer angry emails. In fact, when you have great developer documentation, you may even end up with happy, gushing emails.','To code faster, one has to be efficient; that is, no wasted effort or motion. This can mean everything from typing to tools to thinking. But most of our work as programmers isn’t typing, or compiling—it’s thinking. To think faster, you have to learn more patterns and relationships. This is the knowledge and wisdom that experience builds. What you need to go faster will change over time.','Some developers might think presenting is something super senior and scary. Something for managers or ego-maniacs. But really, presentations are just one way of transmitting ideas and thoughts.','eCommerce has changed the way businesses sell and consumers buy. It grows at a fast pace and it shows no signs of slowing down.  In fact, online retail sales are projected to reach $6 trillion by 2023.','DevOps is the combination of cultural philosophies, practices, and tools that increases an organization’s ability to deliver applications and services at high velocity: evolving and improving products at a faster pace than organizations using traditional software development and infrastructure management processes.','A software code audit is a comprehensive analysis of source code in a programming project with the intent of discovering bugs, security breaches or violations of programming conventions. It is an integral part of the defensive programming paradigm, which attempts to reduce errors before the software is released.',1);");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
