<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220622071339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE services CHANGE documentations documentations LONGTEXT NOT NULL, CHANGE fast_codding fast_codding LONGTEXT NOT NULL, CHANGE online_pressentions online_pressentions LONGTEXT NOT NULL, CHANGE online_shops online_shops LONGTEXT NOT NULL, CHANGE dev_ops dev_ops LONGTEXT NOT NULL, CHANGE audit audit LONGTEXT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE services CHANGE documentations documentations VARCHAR(255) NOT NULL, CHANGE fast_codding fast_codding VARCHAR(255) NOT NULL, CHANGE online_pressentions online_pressentions VARCHAR(255) NOT NULL, CHANGE online_shops online_shops VARCHAR(255) NOT NULL, CHANGE dev_ops dev_ops VARCHAR(255) NOT NULL, CHANGE audit audit VARCHAR(255) NOT NULL');
    }
}
