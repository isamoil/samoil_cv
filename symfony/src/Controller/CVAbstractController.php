<?php

namespace App\Controller;

use App\Entity\About;
use App\Repository\AboutRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class CVAbstractController extends AbstractController
{
    public About|null $user;
    public Session $session;

    public function __construct(
        AboutRepository $aboutRepository,
        RequestStack $requestStack
    ) {
        $this->user = $aboutRepository->getUserData();
        $this->session = $requestStack->getSession();
    }
}
