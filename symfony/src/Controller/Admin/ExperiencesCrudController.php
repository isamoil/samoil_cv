<?php

namespace App\Controller\Admin;

use App\Entity\Experiences;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

class ExperiencesCrudController extends AbstractCrudController
{
    private string $imagesPath;

    public function __construct(string $imagesPath)
    {
        $this->imagesPath = $imagesPath;
    }

    public static function getEntityFqcn(): string
    {
        return Experiences::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [TextField::new('position'),
            TextField::new('period')->setLabel('Start and End Date'),
            TextField::new('company_name'),
            TextField::new('description')->hideOnIndex(),
            AssociationField::new('owner')
                ->setRequired(true)->hideOnIndex(),
            ImageField::new('logo')
                ->hideOnIndex()
                ->setSortable(false)
                ->setLabel('Logo')
                ->setUploadDir($this->imagesPath)
                ->setBasePath(str_replace('/public', '', $this->imagesPath))
                ->setUploadedFileNamePattern('[timestamp].[extension]'),
        ];
    }
}
