<?php

namespace App\Controller\Admin;

use App\Entity\About;
use App\Entity\Educations;
use App\Entity\Experiences;
use App\Entity\Services;
use App\Entity\Skills;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Symfony')
            ->generateRelativeUrls()
            ->disableUrlSignatures();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('About', 'fa fa-user', About::class);
        yield MenuItem::linkToCrud('Skills', 'fa fa-code', Skills::class);
        yield MenuItem::linkToCrud('Services', 'fa fa-wrench', Services::class);
        yield MenuItem::linkToCrud('Educations', 'fa fa-graduation-cap', Educations::class);
        yield MenuItem::linkToCrud('Experiences', 'fa fa-briefcase', Experiences::class);
    }
}
