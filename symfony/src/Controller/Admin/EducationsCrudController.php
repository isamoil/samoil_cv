<?php

namespace App\Controller\Admin;

use App\Entity\Educations;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class EducationsCrudController extends AbstractCrudController
{
    private string $imagesPath;

    public function __construct(string $imagesPath)
    {
        $this->imagesPath = $imagesPath;
    }

    public static function getEntityFqcn(): string
    {
        return Educations::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
//            ...parent::configureFields($pageName),
            TextField::new('start_date'),
            TextField::new('end_date'),
            TextField::new('name'),
            TextField::new('description')->hideOnIndex(),
            AssociationField::new('owner')->hideOnIndex()
                ->setRequired(true),
            ImageField::new('logo')
                ->setSortable(false)
                ->setLabel('Logo')
                ->setUploadDir($this->imagesPath)
                ->setBasePath(str_replace('/public', '', $this->imagesPath))
                ->setUploadedFileNamePattern('[timestamp].[extension]'),
        ];
    }
}
