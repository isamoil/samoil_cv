<?php

namespace App\Controller\Admin;

use App\Entity\Services;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ServicesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Services::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ...parent::configureFields($pageName),
            TextField::new('documentations')->hideOnIndex(),
            TextField::new('fast_codding')->hideOnIndex(),
            TextField::new('online_pressentions')->hideOnIndex(),
            TextField::new('online_shops')->hideOnIndex(),
            TextField::new('devOps')->hideOnIndex(),
            TextField::new('audit')->hideOnIndex(),
            AssociationField::new('owner')->hideOnIndex()
                ->setRequired(true)->hideOnIndex(),
        ];
    }
}
