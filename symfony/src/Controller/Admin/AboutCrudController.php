<?php

namespace App\Controller\Admin;

use App\Entity\About;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class AboutCrudController extends AbstractCrudController
{
    private string $imagesPath;

    public function __construct(string $imagesPath)
    {
        $this->imagesPath = $imagesPath;
    }

    public static function getEntityFqcn(): string
    {
        return About::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ...parent::configureFields($pageName),
            ImageField::new('image')
                ->setSortable(false)
                ->setLabel('Image')
                ->setUploadDir($this->imagesPath)
                ->setBasePath(str_replace('/public', '', $this->imagesPath))
                ->setUploadedFileNamePattern('[timestamp].[extension]'),
        ];
    }
}
