<?php

namespace App\Controller;

use App\Repository\ServicesRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServicesController extends CVAbstractController
{
    #[Route('/services', name: 'app_services')]
    public function index(ServicesRepository $servicesRepository): Response
    {
        $servicesData = $servicesRepository->findOneBy(['owner' => $this->user]);
        return $this->render('page/services/index.html.twig', [
            'services_data' => $servicesData,
            'current_user'   => $this->user,
        ]);
    }
}
