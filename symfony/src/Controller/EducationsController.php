<?php

namespace App\Controller;

use App\Repository\EducationsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EducationsController extends CVAbstractController
{
    #[Route('/educations', name: 'app_educations')]
    public function index(EducationsRepository $educationsRepository): Response
    {
        $educationsData = $educationsRepository->findBy(['owner' => $this->user]);

        return $this->render('page/educations/index.html.twig', [
            'educations_data' => $educationsData,
            'current_user'   => $this->user,
        ]);
    }
}
