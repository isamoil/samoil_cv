<?php

namespace App\Controller;

use App\Repository\ExperiencesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class ExperiencesController extends CVAbstractController
{
    #[Route('/experiences', name: 'app_experiences')]
    public function index(ExperiencesRepository $experiencesRepository): Response
    {
        $experiencesData = $experiencesRepository->findBy(['owner' => $this->user]);

        return $this->render('page/experiences/index.html.twig', [
            'experiences_data' => $experiencesData,
            'current_user'   => $this->user,
        ]);
    }

    private function validateDate($value): bool|string
    {
        if (($timestamp = strtotime($value)) === false) {
            return $value;
        }
            return date('Y', $timestamp);
    }
}
