<?php

namespace App\Controller;

use App\Repository\SkillsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SkillsController extends CVAbstractController
{
    #[Route('/skills', name: 'app_skills')]
    public function index(SkillsRepository $skillsRepository): Response
    {
        $skillsData = $skillsRepository->findOneBy(['owner' => $this->user]);
        return $this->render('page/skills/index.html.twig', [
            'skills_data' => $skillsData,
            'current_user'   => $this->user,
        ]);
    }
}
