<?php

namespace App\Entity;

use App\Repository\ServicesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServicesRepository::class)]
class Services
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

   #[ORM\Column(type: 'text')]
    private $documentations;

   #[ORM\Column(type: 'text')]
    private $fast_codding;

   #[ORM\Column(type: 'text')]
    private $online_pressentions;

   #[ORM\Column(type: 'text')]
    private $online_shops;

   #[ORM\Column(type: 'text')]
    private $devOps;

   #[ORM\Column(type: 'text')]
    private $audit;

   #[ORM\ManyToOne(targetEntity: About::class, inversedBy: 'services')]
   private $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumentations(): ?string
    {
        return $this->documentations;
    }

    public function setDocumentations(string $documentations): self
    {
        $this->documentations = $documentations;

        return $this;
    }

    public function getFastCodding(): ?string
    {
        return $this->fast_codding;
    }

    public function setFastCodding(string $fast_codding): self
    {
        $this->fast_codding = $fast_codding;

        return $this;
    }

    public function getOnlinePressentions(): ?string
    {
        return $this->online_pressentions;
    }

    public function setOnlinePressentions(string $online_pressentions): self
    {
        $this->online_pressentions = $online_pressentions;

        return $this;
    }

    public function getOnlineShops(): ?string
    {
        return $this->online_shops;
    }

    public function setOnlineShops(string $online_shops): self
    {
        $this->online_shops = $online_shops;

        return $this;
    }

    public function getDevOps(): ?string
    {
        return $this->devOps;
    }

    public function setDevOps(string $devOps): self
    {
        $this->devOps = $devOps;

        return $this;
    }

    public function getAudit(): ?string
    {
        return $this->audit;
    }

    public function setAudit(string $audit): self
    {
        $this->audit = $audit;

        return $this;
    }

    public function getOwner(): ?About
    {
        return $this->owner;
    }

    public function setOwner(?About $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
