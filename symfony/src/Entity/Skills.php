<?php

namespace App\Entity;

use App\Repository\SkillsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SkillsRepository::class)]
class Skills
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'integer')]
    private $symfony;

    #[ORM\Column(type: 'integer')]
    private $angular;

    #[ORM\Column(type: 'integer')]
    private $reactJs;

    #[ORM\Column(type: 'integer')]
    private $php;

    #[ORM\Column(type: 'integer')]
    private $js;

    #[ORM\Column(type: 'integer')]
    private $flutter;

    #[ORM\Column(type: 'integer')]
    private $css;

    #[ORM\Column(type: 'integer')]
    private $years_experience;

    #[ORM\Column(type: 'integer')]
    private $folowers_linkedin;

    #[ORM\Column(type: 'integer')]
    private $projects;

    #[ORM\Column(type: 'integer')]
    private $clients;

    #[ORM\ManyToOne(targetEntity: About::class, inversedBy: 'skills')]
    private $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSymfony(): ?int
    {
        return $this->symfony;
    }

    public function setSymfony(int $symfony): self
    {
        $this->symfony = $symfony;

        return $this;
    }

    public function getAngular(): ?int
    {
        return $this->angular;
    }

    public function setAngular(int $angular): self
    {
        $this->angular = $angular;

        return $this;
    }

    public function getReactJs(): ?int
    {
        return $this->reactJs;
    }

    public function setReactJs(int $reactJs): self
    {
        $this->reactJs = $reactJs;

        return $this;
    }

    public function getPhp(): ?int
    {
        return $this->php;
    }

    public function setPhp(int $php): self
    {
        $this->php = $php;

        return $this;
    }

    public function getJs(): ?int
    {
        return $this->js;
    }

    public function setJs(int $js): self
    {
        $this->js = $js;

        return $this;
    }

    public function getFlutter(): ?int
    {
        return $this->flutter;
    }

    public function setFlutter(int $flutter): self
    {
        $this->flutter = $flutter;

        return $this;
    }

    public function getCss(): ?int
    {
        return $this->css;
    }

    public function setCss(int $css): self
    {
        $this->css = $css;

        return $this;
    }

    public function getYearsExperience(): ?int
    {
        return $this->years_experience;
    }

    public function setYearsExperience(int $years_experience): self
    {
        $this->years_experience = $years_experience;

        return $this;
    }

    public function getFolowersLinkedin(): ?int
    {
        return $this->folowers_linkedin;
    }

    public function setFolowersLinkedin(int $folowers_linkedin): self
    {
        $this->folowers_linkedin = $folowers_linkedin;

        return $this;
    }

    public function getProjects(): ?int
    {
        return $this->projects;
    }

    public function setProjects(int $projects): self
    {
        $this->projects = $projects;

        return $this;
    }

    public function getClients(): ?int
    {
        return $this->clients;
    }

    public function setClients(int $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    public function getOwner(): ?About
    {
        return $this->owner;
    }

    public function setOwner(?About $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
